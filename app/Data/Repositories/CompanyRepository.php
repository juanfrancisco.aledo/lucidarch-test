<?php

namespace App\Data\Repositories;

use App\Data\Models\Company;
use Illuminate\Support\Facades\DB;

class CompanyRepository extends Repository
{

    // Constructor to bind model to repo
    public function __construct()
    {
        parent::__construct(new Company());
    }

    /**
     * all
     * @param int $id, Company identifier filter
     * @return Collection(Company)
     */
    public function all($id = null){
        $query = $this->model->query();
        $query->select('id', 'name', 'cif');
        if($id){
            $query->where('id', $id);
        }
        return $query->get();
    }

    /**
     * getById
     * @param int $id, Company identifier filter
     * @return Collection(Company)
     */
    public function getById(int $id){
        $return = null;
        $company = $this->all($id);
        if(!$company->isEmpty()){
            $return = $company->first();
        }
        return $return;
    }

}
