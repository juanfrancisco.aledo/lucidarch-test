<?php

namespace App\Data\Repositories;

use App\Data\Models\User;
use Illuminate\Support\Facades\DB;

class UserRepository extends Repository
{

    // Constructor to bind model to repo
    public function __construct()
    {
        parent::__construct(new User());
    }

    /**
     * all
     * @param int $id, user identifier filter
     * @return Collection(User)
     */
    public function all($id = null){
        $query = $this->model->query();
        $query->select('id', 'name', 'email');
        if($id){
            $query->where('id', $id);
        }
        return $query->get();
    }

    /**
     * getById
     * @param int $id, user identifier filter
     * @return Collection(User)
     */
    public function getById(int $id){
        $return = null;
        $user = $this->all($id);
        if(!$user->isEmpty()){
            $return = $user->first();
        }
        return $return;
    }

    /**
     * delete
     * @param User $user
     * @return Collection(User)
     */
    public function delete(User $user){
        return $user->delete();
    }

    /**
     * getBestBuyers
     * @return Collection(User)
     */
    public function getBestBuyers(){
        $query = $this->model->query();
        $limit = 10;

        $query->distinct()->select('users.id', 'users.name', 'users.email', DB::raw('count(*) as total'));
        $query->leftJoin('orders', 'orders.shop_id', '=', 'users.id');
        $query->groupBy('users.id', 'users.name', 'users.email');
        $query->orderBy('total', 'desc');
        $query->limit($limit);

        return $query->get();
    }

}
