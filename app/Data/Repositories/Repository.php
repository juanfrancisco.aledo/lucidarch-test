<?php

namespace App\Data\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class Repository
{
    // model property on class instances
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

}
