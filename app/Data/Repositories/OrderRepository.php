<?php

namespace App\Data\Repositories;

use App\Data\Models\Order;
use App\Data\Models\Shop;
use App\Data\Models\User;

class OrderRepository extends Repository
{

    // Constructor to bind model to repo
    public function __construct()
    {
        parent::__construct(new Order());
    }

    /**
     * all
     * @param int $pagNum, page number
     * @param int $id, order identifier filter
     * @param int $userId, user identifier filter
     * @param int $shopId, shop identifier filter
     * @return Collection(Order)
     */
    public function all($pagNum = null, $id = null, $userId = null, $shopId = null){
        $query = $this->model->query();

        if($id){
            $query->where('id', $id);
        }
        if($userId){
            $query->where('user_id', $userId);
        }
        if($shopId){
            $query->where('shop_id', $shopId);
        }

        if($pagNum){
            $numElem = 100;
            return $query->paginate($numElem, ['*'], 'page', $pagNum);
        }

        return $query->get();
    }

    /**
     * getOrdersByUserId
     * @param App\Data\Models\User $user
     * @param int $pagNum, page number
     * @return Collection(Order)
     */
    public function getOrdersByUser(User $user, $pagNum = null){
        return $this->all($pagNum, null, $user->id);
    }

    /**
     * getOrdersByUserId
     * @param App\Data\Models\Shop $user
     * @param int $pagNum, page number
     * @return Collection(Order)
     */
    public function getOrdersByShop(Shop $shop, $pagNum = null){
        return $this->all($pagNum, null, null, $shop->id);
    }
}
