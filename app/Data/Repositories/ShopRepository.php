<?php

namespace App\Data\Repositories;

use App\Data\Models\Shop;
use Illuminate\Support\Facades\DB;

class ShopRepository extends Repository
{

    // Constructor to bind model to repo
    public function __construct()
    {
        parent::__construct(new Shop());
    }

    /**
     * all
     * @param int $pagNum, page number
     * @param int $id, shop identifier filter
     * @return Collection(Shop)
     */
    public function all($pagNum = null, $id = null){
        $query = $this->model->query();

        if($id){
            $query->where('id', $id);
        }

        if($pagNum){
            return $query->paginate($pagNum);
        }

        return $query->get();
    }

    /**
     * getById
     * @param int $id, shop identifier filter
     * @return Collection(Shop)
     */
    public function getById(int $id){
        $return = null;
        $shop = $this->all(null, $id);
        if(!$shop->isEmpty()){
            $return = $shop->first();
        }
        return $return;
    }

    /**
     * getBestSeller
     * @return Collection(Shop)
     */
    public function getBestSeller(){
        $query = $this->model->query();
        $limit = 10;

        $query->distinct()->select('shops.id', 'shops.name', 'shops.url', DB::raw('count(*) as total'));
        $query->leftJoin('orders', 'orders.shop_id', '=', 'shops.id');
        $query->groupBy('shops.id', 'shops.name', 'shops.url');
        $query->orderBy('total', 'desc');
        $query->limit($limit);

        return $query->get();
    }


}
