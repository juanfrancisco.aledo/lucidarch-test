<?php

namespace App\Data\Repositories;

use App\Data\Models\Company;
use App\Data\Models\Product;
use App\Data\Models\Shop;
use App\Data\Models\User;
use Illuminate\Support\Facades\DB;

class ProductRepository extends Repository
{

    // Constructor to bind model to repo
    public function __construct()
    {
        parent::__construct(new Product());
    }

    /**
     * all
     * @param int $pagNum, page number
     * @param int $id, product identifier filter
     * @param int $companyId, Company identifier filter
     * @param int $shopId, Shop identifier filter
     * @return Collection(Product)
     */
    public function all($pagNum = null, $id = null, $companyId = null, $shopId = null){
        $query = $this->model->query();

        if($id){
            $query->where('id', $id);
        }
        if($companyId){
            $query->whereHas('companies', function ($query) use ($companyId){
                //company filter
                if($companyId){
                    $query->where('companies.id', '=', $companyId);
                }
            });
        }
        if($shopId){
            $query->whereHas('categories', function ($query) use ($shopId){
                $query->whereHas('shops', function ($query) use ($shopId){
                    //shop filter
                    if($shopId){
                        $query->where('shop.id', '=', $shopId);
                    }
                });
            });
        }


        if($pagNum){
            $numElem = 100;
            return $query->paginate($numElem, ['*'], 'page', $pagNum);
        }

        return $query->get();
    }

    /**
     * getByCompany
     * @param App\Data\Models\Company $company
     * @param int $pagNum, page number
     * @return Collection(Product)
     */
    public function getByCompany(Company $company, $pagNum = null){
        return $this->all($pagNum, null, $company->id);
    }

    /**
     * getByCompany
     * @param App\Data\Models\Shop $shop
     * @return Collection(Product)
     */
    public function getBestSeller(?Shop $shop){
        $limit = 50;
        $query = $this->model->query();

        $query->select('products.id', 'products.name', 'products.slug', DB::raw('IFNULL(sum(order_products.quantity), 0) qt'));
        $query->leftJoin('order_products', 'order_products.product_id', '=', 'products.id');
        $query->groupBy('products.id', 'products.name', 'products.slug');
        $query->orderBy('qt', 'desc');

        if($shop){
            $queryShop = Shop::query();
            $queryShop->select('prods.*')->distinct();
            $queryShop->leftJoin('category_shop', 'category_shop.shop_id', '=', 'shops.id');
            $queryShop->leftJoin('categories', 'categories.id', '=', 'category_shop.category_id');
            $queryShop->leftJoin('category_product', 'category_product.category_id', '=', 'categories.id');

            $queryShop->leftJoinSub($query, 'prods', function ($join) {
                $join->on('prods.id', '=', 'category_product.product_id');
            });

            $queryShop->where('shops.id', $shop->id);
            $queryShop->whereNotNull('prods.id');

            $queryShop->orderBy('prods.qt', 'desc')->get();
            $queryShop->limit($limit);

            $result = $queryShop->get();
        }else{
            $query->limit($limit);
            $result = $query->get();
        }

        return $result;
    }


}
