<?php

namespace App\Domains\Ecommerce\Jobs;

use App\Data\Repositories\ShopRepository;
use Lucid\Units\Job;

class ShopBestSellerJob extends Job
{

    /**
     * @var App\Data\Repositories\ShopRepository;
     */
    protected ShopRepository $repositoryShop;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->repositoryShop = resolve(ShopRepository::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        return $this->repositoryShop->getBestSeller();
    }
}
