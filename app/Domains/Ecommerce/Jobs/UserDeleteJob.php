<?php

namespace App\Domains\Ecommerce\Jobs;

use App\Data\Repositories\OrderRepository;
use App\Data\Repositories\UserRepository;
use App\Exceptions\DependencyException;
use Illuminate\Database\RecordsNotFoundException;
use Lucid\Units\Job;

class UserDeleteJob extends Job
{
    /**
     * @var App\Data\Repositories\UserRepository;
     */
    protected $repositoryUser;

    /**
     * @var App\Data\Repositories\OrderRepository;
     */
    protected $repositoryOrder;

    /**
     * User Identifier
     * @var int
     */
    protected $idUser;

    /**
     * Create a new job instance.
     * @param int $id, user identifier
     * @return void
     */
    public function __construct(int $id)
    {
        $this->repositoryUser = resolve(UserRepository::class);
        $this->repositoryOrder = resolve(OrderRepository::class);
        $this->idUser = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->repositoryUser->getById($this->idUser);
        if(!$user){
            throw new RecordsNotFoundException('This user not exist');
        }
        $orders = $this->repositoryOrder->getOrdersByUser($user);
        if(!$orders->isEmpty()){
            throw new DependencyException('This user has some associated orders');
        }

        return $this->repositoryUser->delete($user);
    }
}
