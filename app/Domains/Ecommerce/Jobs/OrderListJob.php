<?php

namespace App\Domains\Ecommerce\Jobs;

use App\Data\Repositories\OrderRepository;
use App\Data\Repositories\ShopRepository;
use Illuminate\Database\RecordsNotFoundException;
use Lucid\Units\Job;

class OrderListJob extends Job
{
    /**
     * @var int|null
     */
    protected ?int $idShop;

    /**
     * @var int|null
     */
    protected ?int $pagNum;

    /**
     * @var App\Data\Repositories\OrderRepository;
     */
    protected OrderRepository $repositoryOrder;

    /**
     * @var App\Data\Repositories\ShopRepository;
     */
    protected ShopRepository $repositoryShop;

    /**
     * Create a new job instance.
     * @param int $idShop, shop identifier (optional)
     * @return void
     */
    public function __construct(?int $idShop = null, ?int $pagNum = null)
    {
        $this->idShop = $idShop;
        $this->pagNum = $pagNum;
        $this->repositoryOrder = resolve(OrderRepository::class);
        $this->repositoryShop = resolve(ShopRepository::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $shop = null;
        $pag = null;
        if($this->idShop){
            $shop = $this->repositoryShop->getById($this->idShop);
            if(!$shop){
                throw new RecordsNotFoundException('shop not found');
            }
        }

        if($shop){
            $return = $this->repositoryOrder->getOrdersByShop($shop, $this->pagNum);
        }else{
            $return = $this->repositoryOrder->all($this->pagNum);
        }

        return $return;

    }
}
