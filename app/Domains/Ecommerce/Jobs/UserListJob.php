<?php

namespace App\Domains\Ecommerce\Jobs;

use App\Data\Repositories\UserRepository;
use Lucid\Units\Job;

class UserListJob extends Job
{
    protected $repositoryUser;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->repositoryUser = resolve(UserRepository::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        return $this->repositoryUser->all();
    }
}
