<?php

namespace App\Domains\Catalog\Jobs;

use App\Data\Repositories\ProductRepository;
use App\Data\Repositories\ShopRepository;
use Illuminate\Database\RecordsNotFoundException;
use Lucid\Units\Job;

class ProductsBestSellerJob extends Job
{

    /**
     * @var int|null
     */
    protected ?int $idShop;

    /**
     * @var App\Data\Repositories\ProductRepository;
     */
    protected ProductRepository $repositoryProduct;

    /**
     * @var App\Data\Repositories\ShopRepository;
     */
    protected ShopRepository $repositoryShop;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(?int $idShop = null)
    {
        $this->idShop = $idShop;
        $this->repositoryProduct = resolve(ProductRepository::class);
        $this->repositoryShop = resolve(ShopRepository::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $shop = null;
        if($this->idShop){
            $shop = $this->repositoryShop->getById($this->idShop);
            if(!$shop){
                throw new RecordsNotFoundException('Shop not found');
            }
        }

        return $this->repositoryProduct->getBestSeller($shop);

    }
}
