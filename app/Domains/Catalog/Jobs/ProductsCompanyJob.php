<?php

namespace App\Domains\Catalog\Jobs;

use App\Data\Repositories\CompanyRepository;
use App\Data\Repositories\ProductRepository;
use Illuminate\Database\RecordsNotFoundException;
use Lucid\Units\Job;

class ProductsCompanyJob extends Job
{
    /**
     * @var int
     */
    protected int $idCompany;

    /**
     * @var int|null
     */
    protected ?int $pagNum;

    /**
     * @var App\Data\Repositories\ProductRepository;
     */
    protected ProductRepository $repositoryProduct;

    /**
     * @var App\Data\Repositories\CompanyRepository;
     */
    protected CompanyRepository $repositoryCompany;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $idCompany, ?int $pagNum = null)
    {
        $this->idCompany = $idCompany;
        $this->pagNum = $pagNum;
        $this->repositoryProduct = resolve(ProductRepository::class);
        $this->repositoryCompany = resolve(CompanyRepository::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $company = null;
        if($this->idCompany){
            $company = $this->repositoryCompany->getById($this->idCompany);
            if(!$company){
                throw new RecordsNotFoundException('Company not found');
            }
        }

        if($company){
            $return = $this->repositoryProduct->getByCompany($company, $this->pagNum);
        }else{
            $return = $this->repositoryProduct->all($this->pagNum);
        }

        return $return;

    }
}
