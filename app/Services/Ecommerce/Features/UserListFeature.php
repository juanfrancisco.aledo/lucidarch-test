<?php

namespace App\Services\Ecommerce\Features;

use App\Domains\Ecommerce\Jobs\UserListJob;
use Illuminate\Http\Request;
use Lucid\Units\Feature;

class UserListFeature extends Feature
{
    public function handle(Request $request)
    {
        $users = $this->run(new UserListJob());

        return $users;
    }
}
