<?php

namespace App\Services\Ecommerce\Features;

use App\Domains\Ecommerce\Jobs\UserBestBuyersJob;
use Illuminate\Http\Request;
use Lucid\Units\Feature;

class UserBestBuyersFeature extends Feature
{
    public function handle(Request $request)
    {
        return $this->run(new UserBestBuyersJob());
    }
}
