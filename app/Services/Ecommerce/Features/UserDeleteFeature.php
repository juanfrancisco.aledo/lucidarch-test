<?php

namespace App\Services\Ecommerce\Features;

use App\Domains\Ecommerce\Jobs\UserDeleteJob;
use App\Exceptions\DependencyException;
use Illuminate\Database\RecordsNotFoundException;
use Illuminate\Http\Request;
use Lucid\Units\Feature;

class UserDeleteFeature extends Feature
{
    /**
     * @var int $id
     */
    protected int $id;

    //constructor
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function handle(Request $request)
    {
        $data = false;
        try{
            $data = $this->run(new UserDeleteJob($this->id));
        }catch (DependencyException $e){
            $data = $e->getMessage();
        }catch (RecordsNotFoundException $e){
            $data = $e->getMessage();
        }


        return $data;
    }
}
