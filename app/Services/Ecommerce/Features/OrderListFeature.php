<?php

namespace App\Services\Ecommerce\Features;

use App\Domains\Ecommerce\Jobs\OrderListJob;
use Illuminate\Database\RecordsNotFoundException;
use Illuminate\Http\Request;
use Lucid\Units\Feature;

class OrderListFeature extends Feature
{
    /**
     * Shop identifier
     * @var int|null $idShop
     */
    protected ?int $idShop;

    //constructor
    public function __construct(?int $idShop = null)
    {
        $this->idShop = $idShop;
    }

    public function handle(Request $request)
    {
        $pagNum = ($request->input('pagNum')) ? $request->input('pagNum') : 1;

        $data = null;
        try{
            $data = $this->run(new OrderListJob($this->idShop, $pagNum));
        }catch (RecordsNotFoundException $e){
            $data = $e->getMessage();
        }

        return $data;
    }
}
