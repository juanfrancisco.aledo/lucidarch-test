<?php

namespace App\Services\Ecommerce\Features;

use App\Domains\Ecommerce\Jobs\ShopBestSellerJob;
use Illuminate\Http\Request;
use Lucid\Units\Feature;

class ShopBestSellerFeature extends Feature
{
    public function handle(Request $request)
    {
        return $this->run(new ShopBestSellerJob());
    }
}
