<?php

/*
|--------------------------------------------------------------------------
| Service - API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Prefix: /api/ecommerce
Route::group(['prefix' => 'ecommerce'], function() {

    // Controllers live in src/Services/Ecommerce/Http/Controllers

    Route::get('/', function() {
        return response()->json(['path' => '/api/ecommerce']);
    });

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    Route::get('/users', 'UserController@index');
    Route::delete('/users/{id}/delete', 'UserController@destroy');
    Route::get('/users/best-buyers', 'UserController@bestBuyers');


    Route::get('/orders/shop/{idShop?}', 'OrderController@listOrders');
    Route::get('/shops/best-seller', 'ShopController@bestSellerShops');

});
