<?php

namespace App\Services\Ecommerce\Http\Controllers;

use App\Services\Ecommerce\Features\UserBestBuyersFeature;
use App\Services\Ecommerce\Features\UserDeleteFeature;
use App\Services\Ecommerce\Features\UserListFeature;
use Illuminate\Http\Request;
use Lucid\Units\Controller;

class UserController extends Controller
{
    /**
     * index
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return $this->serve(UserListFeature::class);
    }

    /**
     * destroy
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        return $this->serve(UserDeleteFeature::class, ['id' => $id]);
    }

    /**
     * bestBuyers
     * @return \Illuminate\Http\Response
     */
    public function bestBuyers(){
        return $this->serve(UserBestBuyersFeature::class);
    }

}
