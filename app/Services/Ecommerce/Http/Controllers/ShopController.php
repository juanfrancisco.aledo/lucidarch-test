<?php

namespace App\Services\Ecommerce\Http\Controllers;

use App\Services\Ecommerce\Features\ShopBestSellerFeature;
use Lucid\Units\Controller;

class ShopController extends Controller
{
    /**
     * listOrders
     * @param int|null $idShop
     * @return \Illuminate\Http\Response
     */
    public function bestSellerShops(?int $idShop = null){
        return $this->serve(ShopBestSellerFeature::class, ['idShop' => $idShop]);
    }

}
