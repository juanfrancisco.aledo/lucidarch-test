<?php

namespace App\Services\Ecommerce\Http\Controllers;

use App\Services\Ecommerce\Features\OrderListFeature;
use Lucid\Units\Controller;

class OrderController extends Controller
{

    /**
     * listOrders
     * @param int|null $idShop
     * @return \Illuminate\Http\Response
     */
    public function listOrders(?int $idShop = null){
        return $this->serve(OrderListFeature::class, ['idShop' => $idShop]);
    }


}
