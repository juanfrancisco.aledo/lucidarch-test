<?php

namespace App\Services\Catalog\Http\Controllers;

use App\Services\Catalog\Features\ProductsBestSellerFeature;
use App\Services\Catalog\Features\ProductsCompanyFeature;
use Lucid\Units\Controller;

class ProductController extends Controller
{
    /**
     * getProductsByCompany
     * @param int $id, Company Identifier
     * @return \Illuminate\Http\Response
     */
    public function getProductsByCompany(int $id){
        return $this->serve(ProductsCompanyFeature::class, ['idCompany' => $id]);
    }

    /**
     * bestSeller
     * @param int|null $idShop
     * @return \Illuminate\Http\Response
     */
    public function bestSeller(?int $idShop = null){
        return $this->serve(ProductsBestSellerFeature::class, ['idShop' => $idShop]);
    }
}
