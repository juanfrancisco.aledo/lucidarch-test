<?php

namespace App\Services\Catalog\Features;

use App\Domains\Catalog\Jobs\ProductsBestSellerJob;
use Illuminate\Database\RecordsNotFoundException;
use Illuminate\Http\Request;
use Lucid\Units\Feature;

class ProductsBestSellerFeature extends Feature
{
    /**
     * Shop identifier
     * @var int|null $idShop
     */
    protected ?int $idShop;

    //constructor
    public function __construct(?int $idShop = null)
    {
        $this->idShop = $idShop;
    }

    public function handle(Request $request)
    {
        $data = null;
        try{
            $data = $this->run(new ProductsBestSellerJob($this->idShop));
        }catch (RecordsNotFoundException $e){
            $data = $e->getMessage();
        }

        return $data;
    }
}
