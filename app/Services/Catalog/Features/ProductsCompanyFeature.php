<?php

namespace App\Services\Catalog\Features;

use App\Domains\Catalog\Jobs\ProductsCompanyJob;
use Illuminate\Database\RecordsNotFoundException;
use Illuminate\Http\Request;
use Lucid\Units\Feature;

class ProductsCompanyFeature extends Feature
{

    /**
     * Company identifier
     * @var int $idCompany
     */
    protected int $idCompany;

    //constructor
    public function __construct(int $idCompany)
    {
        $this->idCompany = $idCompany;
    }

    public function handle(Request $request)
    {
        $pagNum = ($request->input('pagNum')) ? $request->input('pagNum') : 1;

        $data = null;
        try{
            $data = $this->run(new ProductsCompanyJob($this->idCompany, $pagNum));
        }catch (RecordsNotFoundException $e){
            $data = $e->getMessage();
        }

        return $data;
    }

}
