<?php

namespace Database\Factories;

use App\Data\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->name();
        return [
            'name' => $name,
            'slug' => $this->faker->unique()->slug(),
            'description' => 'Product '.$name.' description',
            'price' => $this->faker->randomFloat(null, 1, 1000),
        ];
    }
}
