<?php

namespace Database\Seeders;

use App\Data\Models\Order;
use App\Data\Models\Shop;
use App\Data\Models\User;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Order::factory()
        ->for(Shop::factory())
        ->for(User::factory())
        ->count(10)
        ->create();
    }
}
