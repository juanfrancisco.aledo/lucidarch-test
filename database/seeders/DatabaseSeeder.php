<?php

namespace Database\Seeders;

use App\Data\Models\Category;
use App\Data\Models\Company;
use App\Data\Models\CompanyContact;
use App\Data\Models\Order;
use App\Data\Models\OrderProduct;
use App\Data\Models\Product;
use App\Data\Models\Shop;
use App\Data\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // Create Companies and Contact
        $companies = Company::factory()->count(100)->create();
        foreach($companies as $company){
            CompanyContact::factory()
                ->for($company)
                ->count(rand(1, 3))
                ->create();
        }

        // Create Categories
        $shops = Shop::factory()->count(10)->create();
        $categories = collect();

        for($i=0; $i<10; $i++){
            $categories->push(Category::factory()
                ->hasAttached($shops->random(3))
                ->create());
        }

        //Create products
        $products = collect();
        foreach($categories as $category){

            $products = $products->merge(Product::factory()
                ->hasAttached($category)
                ->hasAttached($companies->random(3))
                ->count(rand(5, 20))
                ->create());

        }


        //Create Orders
        $users = User::factory()->count(100)->create();
        $users = $users->random(20);
        foreach($users as $user){
            $orders = Order::factory()
                ->for($shops->random(1)->first())
                ->for($user)
                ->count(rand(1, 5))
                ->create();
            //Create Order Lines
            foreach($orders as $order){
                $prods = $products->random(3); //Get random products
                foreach($prods as $prod){
                    OrderProduct::factory()
                    ->for($order)
                    ->for($prod)
                    ->create();
                }
            }
        }


    }
}
