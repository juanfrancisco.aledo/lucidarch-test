<?php

namespace Database\Seeders;

use App\Data\Models\Category;
use App\Data\Models\Product;
use App\Data\Models\Shop;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::factory()
        ->has(Shop::factory())
        ->has(Product::factory())
        ->create();
    }
}
