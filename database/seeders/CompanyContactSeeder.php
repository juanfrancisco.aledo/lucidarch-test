<?php

namespace Database\Seeders;

use App\Data\Models\Company;
use App\Data\Models\CompanyContact;
use Illuminate\Database\Seeder;

class CompanyContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CompanyContact::factory()
        ->for(Company::factory())
        ->count(10)
        ->create();
    }
}
