<?php

namespace Database\Seeders;

use App\Data\Models\Order;
use App\Data\Models\OrderProduct;
use App\Data\Models\Product;
use App\Data\Models\Shop;
use App\Data\Models\User;
use Illuminate\Database\Seeder;

class OrderProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderProduct::factory()
        ->for(
            Order::factory()
            ->for(Shop::factory())
            ->for(User::factory())
        )
        ->for(
            Product::factory()
        )
        ->count(10)
        ->create();
    }
}
